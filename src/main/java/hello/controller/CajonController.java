package hello.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hello.entity.Cajon;
import hello.model.MCajon;
import hello.service.CajonService;

@RestController
@RequestMapping("/cajon")
public class CajonController {
	@Autowired
    @Qualifier("servicioCajon")
    CajonService service;

    @GetMapping("/cajones")
    public @ResponseBody List<MCajon> getallBodegas(Pageable pageable){
        return  service.obtener(pageable);
    }

    @PutMapping("/add")
    public boolean agregarCajon(@RequestBody @Valid Cajon cajon){
        return service.crear(cajon);
    }

    @GetMapping("/libres")
    public @ResponseBody List<MCajon> getallBodegaslibres(Pageable pageable){
        return service.obtener_libres(false,pageable);
    }
    
    /*No funca*/
    @GetMapping("/porid")
    @ResponseBody
    public Cajon getCajonEspecifico(@RequestParam long id) {
    	return service.obtenerPorId(id);
    }
    
    @PostMapping("/add")
    public boolean actualizarCajon(@RequestBody @Valid Cajon cajon){
        return service.actualizar(cajon);
    }

}


