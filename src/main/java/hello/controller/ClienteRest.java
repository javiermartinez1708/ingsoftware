package hello.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import hello.entity.Bodega;

import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;


@Controller
@RequestMapping("/forms")
public class ClienteRest {

    @PostMapping("/agregar")
    public boolean agregar(Bodega bodega){

        ModelAndView mav = new ModelAndView("forms");
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<Bodega[]> bodegaEntity = rest.exchange("http://localhost:8000/api/bodega/add", HttpMethod.PUT, entity, Bodega[].class);
        Bodega[] notas = bodegaEntity.getBody();
        mav.addObject("notas", notas);

		return false;

    }
}

