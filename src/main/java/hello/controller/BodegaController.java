package hello.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Pageable;


import java.util.List;

import hello.entity.Bodega;
import hello.model.MBodega;
import hello.service.BodegaService;

@RestController
@RequestMapping("/bodega")
public class BodegaController {
    @Autowired
    @Qualifier("servicioBodega")
    BodegaService service;

    @GetMapping("/bodegas")
    public @ResponseBody List<MBodega> getallBodegas(Pageable pageable){
        return  service.obtener(pageable);
    }

    @PutMapping("/add")
    public boolean agregarBodega(@RequestBody @Valid Bodega bodega){
        return service.crear(bodega);
    }

    @GetMapping("/libres")
    public @ResponseBody List<MBodega> getallBodegaslibres(Pageable pageable){
        return service.obtener_libres(false,pageable);
    }
}
