package hello.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Table(name= "Proyecto")
@Entity
public class Proyecto implements Serializable{
    public Proyecto(){

    }
    public Proyecto(long id, String Nombre, String NombreGrupo,String JefeGrupo,long Maquina,boolean Ocupada){
        this.id = id;
        this.Nombre = Nombre;
        this.NombreGrupo = NombreGrupo;
        this.JefeGrupo = JefeGrupo;
        this.Maquina = Maquina;
        this.Ocupada = Ocupada;
    }

    @Id
    @Column(name="id")
    private long id;
    @Column(name="Nombre")
    private String Nombre;
    @Column(name="NombreGrupo")
    private String NombreGrupo;
    @Column(name="JefeGrupo")
    private String JefeGrupo;
    @Column(name="Maquina")
    private long Maquina;
    @Column(name="Ocupada")
    private boolean Ocupada;

    public long getid(){
        return  id;
    }
    public void setid(long id){
        this.id = id;
    }
    public String getNombre(){
        return Nombre;
    }
    public void setNombre(String Nombre){
        this.Nombre = Nombre;
    }
    public String getNombreGrupo(){
        return NombreGrupo;
    }
    public void setNombreGrupo(String NombreGrupo){
        this.NombreGrupo = NombreGrupo;
    }
    public String getJefeGrupo(){
        return JefeGrupo;
    }
    public void setJefeGrupo(String JefeGrupo){
        this.JefeGrupo = JefeGrupo;
    }
    public long getMaquina(){
        return Maquina;
    }
    public void setMaquina(long Maquina){
        this.Maquina = Maquina;
    }
    public boolean getOcupada(){
        return Ocupada;
    }
    public void setOcupada(boolean Ocupada){
        this.Ocupada = Ocupada;
    }
}