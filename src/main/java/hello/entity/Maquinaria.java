package hello.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Table(name= "Maquinaria")
@Entity
public class Maquinaria implements Serializable{
    public Maquinaria(){

    }
    public Maquinaria(String Tipo, long Instancia, String GrupoAsignado,boolean Asignada){
        this.Tipo = Tipo;
        this.Instancia = Instancia;
        this.Asignada = Asignada;
        this.GrupoAsignado = GrupoAsignado;
    }
    @Id
    @Column(name="Tipo")
    private String Tipo;
    @Column(name="Instancia")
    private long Instancia;
    @Column(name="GrupoAsignado")
    private String GrupoAsignado;
    @Column(name = "Asignada")
    private boolean Asignada;

    public String getTipo(){
        return Tipo;
    }
    public void setTipo(String Tipo){
        this.Tipo = Tipo;
    }
    public long getInstancia(){
        return Instancia;
    }
    public void setInstancia(long Instancia){
        this.Instancia = Instancia;
    }
    public String getGrupoAsignado(){
        return GrupoAsignado;
    }
    public void setGrupoAsignado(String GrupoAsignado){
        this.GrupoAsignado = GrupoAsignado;
    }
    public boolean getAsignada(){
        return Asignada;
    }
    public void setAsignada(boolean Asignada){
        this.Asignada = Asignada;
    }
}