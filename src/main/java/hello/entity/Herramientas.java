package hello.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Table(name= "Herramientas")
@Entity
public class Herramientas implements Serializable{
    public Herramientas(){

    }
    public Herramientas(long id, String Nombre, long Cantidad){
        this.id = id;
        this.Nombre = Nombre;
        this.Cantidad = Cantidad;
    }
    @Id
    @Column(name="id")
    private long id;
    @Column(name="Nombre")
    private String Nombre;
    @Column(name="Cantidad")
    private long Cantidad;

    public long getid(){
        return  id;
    }
    public void setid(long id){
        this.id = id;
    }
    public String getNombre(){
        return Nombre;
    }
    public void setNombre(String Nombre){
        this.Nombre = Nombre;
    }
    public long getCantidad(){
        return Cantidad;
    }
    public void setCantidad(long Cantidad){
        this.Cantidad = Cantidad;
    }
}