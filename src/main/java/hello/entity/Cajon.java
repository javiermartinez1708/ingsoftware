package hello.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;


@Table(name= "Cajon")
@Entity
public class Cajon implements Serializable{
    public Cajon(){

    }
    public Cajon(long id, String Dueno, Date Fechainic,Date FechaFinal,boolean ocupada){
        this.id = id;
        this.Dueno = Dueno;
        this.Fechainic = Fechainic;
        this.FechaFinal = FechaFinal;
    }
    @Id
    @Column(name="id")
    private long id;
    @Column(name="Dueno")
    private String Dueno;
    @Column(name="Fechainic")
    private Date Fechainic;
    @Column(name="FechaFinal")
    private Date FechaFinal;
    @Column(name="ocupada")
    private boolean ocupada;

    public long getid(){
        return  id;
    }
    public void setid(long id){
        this.id = id;
    }
    public String getDueno(){
        return Dueno;
    }
    public void setDueno(String Dueno){
        this.Dueno = Dueno;
    }
    public Date getFechainic(){
        return Fechainic;
    }
    public void setFechainic(Date Fechainic){
        this.Fechainic = Fechainic;
    }
    public Date getFechaFinal(){
        return FechaFinal;
    }
    public void setFechaFinal(Date FechaFinal){
        this.FechaFinal = FechaFinal;
    }
    public boolean getOcupada(){
        return ocupada;
    }
    public void setOcupada(boolean ocupada){
        this.ocupada = ocupada;
    }
}