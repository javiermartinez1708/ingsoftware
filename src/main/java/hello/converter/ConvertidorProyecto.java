package hello.converter;

import java.util.List;
import java.util.ArrayList;
import org.springframework.stereotype.Component;

import hello.entity.Proyecto;
import hello.model.MProyecto;

@Component("ConProyecto")
public class ConvertidorProyecto {
    public List<MProyecto> convertirLista(List<Proyecto> proyectos){
        List<MProyecto> mproyectos = new ArrayList<>();
        for(Proyecto proyecto : proyectos){
            mproyectos.add(new MProyecto(proyecto));
        }
        return mproyectos;
    }
}