package hello.converter;

import java.util.List;
import java.util.ArrayList;
import org.springframework.stereotype.Component;

import hello.entity.MateriaPrima;
import hello.model.MMateriaPrima;

@Component("ConMateriaPrima")
public class ConvertidorMateriaPrima {
    public List<MMateriaPrima> convertirLista(List<MateriaPrima> materiaprimas){
        List<MMateriaPrima> mmateriaprimas = new ArrayList<>();
        for(MateriaPrima materiaprima : materiaprimas){
            mmateriaprimas.add(new MMateriaPrima(materiaprima));
        }
        return mmateriaprimas;
    }
}