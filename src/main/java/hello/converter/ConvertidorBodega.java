package hello.converter;

import java.util.List;
import java.util.ArrayList;
import org.springframework.stereotype.Component;

import hello.entity.Bodega;
import hello.model.MBodega;

@Component("ConBodega")
public class ConvertidorBodega {
    public List<MBodega> convertirLista(List<Bodega> bodegas){
        List<MBodega> mbodegas = new ArrayList<>();
        for(Bodega bodega : bodegas){
            mbodegas.add(new MBodega(bodega));
        }
        return mbodegas;
    }
}