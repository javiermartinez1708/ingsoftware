package hello.converter;

import java.util.List;
import java.util.ArrayList;
import org.springframework.stereotype.Component;

import hello.entity.Maquinaria;
import hello.model.MMaquinaria;

@Component("ConMaquinaria")
public class ConvertidorMaquinaria {
    public List<MMaquinaria> convertirLista(List<Maquinaria> maquinarias){
        List<MMaquinaria> mmaquinarias = new ArrayList<>();
        for(Maquinaria maquinaria : maquinarias){
            mmaquinarias.add(new MMaquinaria(maquinaria));
        }
        return mmaquinarias;
    }
}