package hello.converter;

import java.util.List;
import java.util.ArrayList;
import org.springframework.stereotype.Component;

import hello.entity.Cajon;
import hello.model.MCajon;

@Component("ConCajon")
public class ConvertidorCajon {
    public List<MCajon> convertirLista(List<Cajon> cajones){
        List<MCajon> mcajones = new ArrayList<>();
        for(Cajon cajon : cajones){
            mcajones.add(new MCajon(cajon));
        }
        return mcajones;
    }
}