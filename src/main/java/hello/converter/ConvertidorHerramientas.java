package hello.converter;

import java.util.List;
import java.util.ArrayList;
import org.springframework.stereotype.Component;

import hello.entity.Herramientas;
import hello.model.MHerramientas;

@Component("ConHerramientas")
public class ConvertidorHerramientas {
    public List<MHerramientas> convertirLista(List<Herramientas> herramientas){
        List<MHerramientas> mherramientas = new ArrayList<>();
        for(Herramientas herramienta : herramientas){
            mherramientas.add(new MHerramientas(herramienta));
        }
        return mherramientas;
    }
}