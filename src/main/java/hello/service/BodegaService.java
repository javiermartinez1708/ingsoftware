package hello.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import hello.converter.ConvertidorBodega;
import hello.entity.Bodega;
import hello.model.MBodega;
import hello.repository.BodegaRepositorio;

import java.util.List;

@Service("servicioBodega")
public class BodegaService{
    @Autowired
    @Qualifier("repositorioBodega")
    private BodegaRepositorio repositorio;

    @Autowired
    @Qualifier("ConBodega")
    private ConvertidorBodega convertidor;

    public boolean crear(Bodega bodega){
        try{
            repositorio.save(bodega);
            return true;
        }catch(Exception e){
            return false;
        }
    }
    public boolean actualizar(Bodega bodega){
        try{
            repositorio.save(bodega);
            return true;
        }catch(Exception e){
            return false;
        }
    }
    public List<MBodega> obtener(Pageable pageable){
        return convertidor.convertirLista(repositorio.findAll(pageable).getContent());
    }
    /* get bodega desocupada */
    public List<MBodega> obtener_libres( boolean ocupada, Pageable pageable){
        return convertidor.convertirLista(repositorio.findByocupada(ocupada, pageable).getContent());
    }
}