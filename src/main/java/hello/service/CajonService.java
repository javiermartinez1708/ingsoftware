package hello.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import hello.converter.ConvertidorCajon;
import hello.entity.Cajon;
import hello.model.MCajon;
import hello.repository.CajonRepositorio;

@Service("servicioCajon")
public class CajonService {
	@Autowired
	@Qualifier("repositorioCajon")
	private CajonRepositorio repositorio;
	
	@Autowired
    @Qualifier("ConCajon")
    private ConvertidorCajon convertidor;
	
	private static final Log logger = LogFactory.getLog(CajonService.class);
	
	public boolean crear(Cajon cajon){
		logger.info("Creando nuevo cajon");
        try{
            repositorio.save(cajon);
            logger.info("Cajon creado");
            return true;
        }catch(Exception e){
        	logger.info("No es posible crear un nuevo cajon");
            return false;
        }
    }
    public boolean actualizar(Cajon cajon){
    	logger.info("Modificando cajon");
        try{
            repositorio.save(cajon);
            logger.info("Cajon modificado");
            return true;
        }catch(Exception e){
        	logger.info("Hubo un error al modificar el cajon");
            return false;
        }
    }
    public List<MCajon> obtener(Pageable pageable){
        return convertidor.convertirLista(repositorio.findAll(pageable).getContent());
    }
    /* get cajon desocupada */
    public List<MCajon> obtener_libres( boolean ocupada, Pageable pageable){
        return convertidor.convertirLista(repositorio.findByocupada(ocupada, pageable).getContent());
    }
    
    /*VER SI FUNCA*/
    public Cajon obtenerPorId(long id) {
    	return repositorio.findById(id);
    }

}
