package hello.repository;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hello.entity.Cajon;

@Repository("repositorioCajon")
public interface CajonRepositorio extends JpaRepository<Cajon, Serializable>,
	PagingAndSortingRepository<Cajon, Serializable>{
	
	public abstract Cajon findById(long id);
	
	public abstract Page<Cajon> findAll(Pageable pageable);
	
	public abstract Page<Cajon> findByocupada(boolean Ocupada, Pageable pageable);
}