package hello.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

import hello.entity.Bodega;

@Repository("repositorioBodega")
public interface BodegaRepositorio extends JpaRepository<Bodega, Serializable>,
    PagingAndSortingRepository<Bodega, Serializable>{

        public abstract Page<Bodega> findAll(Pageable pageable);
        
        public abstract Page<Bodega> findByocupada(boolean Ocupada, Pageable pageable);
}