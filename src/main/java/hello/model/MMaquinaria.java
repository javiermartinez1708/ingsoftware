package hello.model;

import java.io.Serializable;
import hello.entity.Maquinaria;

public class MMaquinaria implements Serializable{
    public MMaquinaria(){

    }
    public MMaquinaria(Maquinaria Maquinaria){
        this.Tipo = Maquinaria.getTipo();
        this.Instancia = Maquinaria.getInstancia();
        this.Asignada = Maquinaria.getAsignada();
        this.GrupoAsignado = Maquinaria.getGrupoAsignado();
    }
    public MMaquinaria(String Tipo, long Instancia, String GrupoAsignado,boolean Asignada){
        this.Tipo = Tipo;
        this.Instancia = Instancia;
        this.Asignada = Asignada;
        this.GrupoAsignado = GrupoAsignado;
    }

    private String Tipo;

    private long Instancia;

    private String GrupoAsignado;

    private boolean Asignada;

    public String getTipo(){
        return Tipo;
    }
    public void setTipo(String Tipo){
        this.Tipo = Tipo;
    }
    public long getInstancia(){
        return Instancia;
    }
    public void setInstancia(long Instancia){
        this.Instancia = Instancia;
    }
    public String getGrupoAsignado(){
        return GrupoAsignado;
    }
    public void setGrupoAsignado(String GrupoAsignado){
        this.GrupoAsignado = GrupoAsignado;
    }
    public boolean getAsignada(){
        return Asignada;
    }
    public void setAsignada(boolean Asignada){
        this.Asignada = Asignada;
    }
}