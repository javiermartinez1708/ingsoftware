package hello.model;


import java.io.Serializable;
import java.util.Date;

import hello.entity.Bodega;

public class MBodega implements Serializable{
    public MBodega(){
    }
    public MBodega(Bodega Bodega){
        this.id = Bodega.getid();
        this.Dueno = Bodega.getDueno();
        this.Fechainic = Bodega.getFechainic();
        this.FechaFinal = Bodega.getFechaFinal();
        this.Ocupada = Bodega.getocupada();
    }
    public MBodega(long id, String Dueno, Date Fechainic,Date FechaFinal, boolean Ocupada){
        this.id = id;
        this.Dueno = Dueno;
        this.Fechainic = Fechainic;
        this.FechaFinal = FechaFinal;
        this.Ocupada = Ocupada;
    }

    private long id;
    private String Dueno;
    private Date Fechainic;
    private Date FechaFinal;
    private boolean Ocupada;

    public long getid(){
        return  id;
    }
    public void setid(long id){
        this.id = id;
    }
    public String getDueno(){
        return Dueno;
    }
    public void setDueno(String Dueno){
        this.Dueno = Dueno;
    }
    public Date getFechainic(){
        return Fechainic;
    }
    public void setFechainic(Date Fechainic){
        this.Fechainic = Fechainic;
    }
    public Date getFechaFinal(){
        return FechaFinal;
    }
    public void setFechaFinal(Date FechaFinal){
        this.FechaFinal = FechaFinal;
    }
    public boolean getOcupada(){
        return Ocupada;
    }
    public void setOcupada(boolean Ocupada){
        this.Ocupada = Ocupada;
    }
}