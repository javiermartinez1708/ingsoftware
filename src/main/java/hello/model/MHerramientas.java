package hello.model;

import java.io.Serializable;
import hello.entity.Herramientas;



public class MHerramientas implements Serializable{
    public MHerramientas(){

    }
    public MHerramientas(Herramientas Herramientas){
        this.id = Herramientas.getid();
        this.Nombre = Herramientas.getNombre();
        this.Cantidad = Herramientas.getCantidad();
    }
    
    public MHerramientas(long id, String Nombre, long Cantidad){
        this.id = id;
        this.Nombre = Nombre;
        this.Cantidad = Cantidad;
    }

    private long id;
    private String Nombre;
    private long Cantidad;

    public long getid(){
        return  id;
    }
    public void setid(long id){
        this.id = id;
    }
    public String getNombre(){
        return Nombre;
    }
    public void setNombre(String Nombre){
        this.Nombre = Nombre;
    }
    public long getCantidad(){
        return Cantidad;
    }
    public void setCantidad(long Cantidad){
        this.Cantidad = Cantidad;
    }
}