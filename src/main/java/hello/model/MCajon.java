package hello.model;

import java.io.Serializable;
import java.util.Date;

import hello.entity.Cajon;

public class MCajon implements Serializable{
    public MCajon(){

    }
    public MCajon(Cajon Cajon){
        this.id = Cajon.getid();
        this.Dueno = Cajon.getDueno();
        this.Fechainic = Cajon.getFechainic();
        this.FechaFinal = Cajon.getFechaFinal();
        this.ocupada = Cajon.getOcupada();
    }

    public MCajon(long id, String Dueno, Date Fechainic,Date FechaFinal,boolean ocupada){
        this.id = id;
        this.Dueno = Dueno;
        this.Fechainic = Fechainic;
        this.FechaFinal = FechaFinal;
        this.ocupada = ocupada;
    }

    private long id;
    private String Dueno;
    private Date Fechainic;
    private Date FechaFinal;
    private boolean ocupada;

    public long getid(){
        return  id;
    }
    public void setid(long id){
        this.id = id;
    }
    public String getDueno(){
        return Dueno;
    }
    public void setDueno(String Dueno){
        this.Dueno = Dueno;
    }
    public Date getFechainic(){
        return Fechainic;
    }
    public void setFechainic(Date Fechainic){
        this.Fechainic = Fechainic;
    }
    public Date getFechaFinal(){
        return FechaFinal;
    }
    public void setFechaFinal(Date FechaFinal){
        this.FechaFinal = FechaFinal;
    }
    public boolean getOcupada(){
        return ocupada;
    }
    public void setOcupada(boolean ocupada){
        this.ocupada = ocupada;
    }
}