package hello.model;

import java.io.Serializable;
import hello.entity.Proyecto;


public class MProyecto implements Serializable{
    public MProyecto(){

    }
    public MProyecto(Proyecto Proyecto){
        this.id = Proyecto.getid();
        this.Nombre = Proyecto.getNombre();
        this.NombreGrupo = Proyecto.getNombreGrupo();
        this.JefeGrupo = Proyecto.getJefeGrupo();
        this.Maquina = Proyecto.getMaquina();
        this.Ocupada = Proyecto.getOcupada();
    }
    public MProyecto(long id, String Nombre, String NombreGrupo,String JefeGrupo,long Maquina,boolean Ocupada){
        this.id = id;
        this.Nombre = Nombre;
        this.NombreGrupo = NombreGrupo;
        this.JefeGrupo = JefeGrupo;
        this.Maquina = Maquina;
        this.Ocupada = Ocupada;
    }


    private long id;
    private String Nombre;
    private String NombreGrupo;
    private String JefeGrupo;
    private long Maquina;
    private boolean Ocupada;

    public long getid(){
        return  id;
    }
    public void setid(long id){
        this.id = id;
    }
    public String getNombre(){
        return Nombre;
    }
    public void setNombre(String Nombre){
        this.Nombre = Nombre;
    }
    public String getNombreGrupo(){
        return NombreGrupo;
    }
    public void setNombreGrupo(String NombreGrupo){
        this.NombreGrupo = NombreGrupo;
    }
    public String getJefeGrupo(){
        return JefeGrupo;
    }
    public void setJefeGrupo(String JefeGrupo){
        this.JefeGrupo = JefeGrupo;
    }
    public long getMaquina(){
        return Maquina;
    }
    public void setMaquina(long Maquina){
        this.Maquina = Maquina;
    }
    public boolean getOcupada(){
        return Ocupada;
    }
    public void setOcupada(boolean Ocupada){
        this.Ocupada = Ocupada;
    }
}